# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# All rights reserved

import math
import typing

import dpkt
import numpy as np

from ska_low_cbf_model import lfaa_spead
from ska_low_cbf_model.lfaa_spead import complex_int8, lfaa_spead_header

# clock speed for the 100G hard core in the firmware, in MHz
ETH_CLK = 322
# Line rate for the 100G input in Gbit/sec
ETH_RATE = 100


def check_lfaa_header(header: lfaa_spead_header, payload: bytes):
    """
    :param header: the LFAA SPEAD header to check
    :param payload: the LFAA SPEAD payload (to check length matches header)
    :raises AssertionError: If any check fails
    Returns the condensed header information used in the virtual channel table.
    """

    assert header["magic"] == 0x53, "SPEAD magic number is wrong"
    assert header["version"] == 4, "SPEAD version should be 4"
    assert (
        header["item_pointer_width"] == 2
    ), "SPEAD item pointer width field should be 2"
    assert header["N_items"] == 8, "SPEAD number of header items should be 8"
    assert len(payload) == 8192, "SPEAD payload should be 8192 bytes"
    # could also validate/decode other LFAA SPEAD header fields

    # Get the fields that are used to lookup the virtual channel.
    #  bits 2:0 = substation_id,
    #  bits 12:3 = station_id,
    #  bits 16:13 = beam_id,
    #  bits 25:17 = frequency_id
    #  bits 30:26 = subarray_id
    #  bit  31 = set to '1' to indicate this entry is valid
    assert (
        header["csp_substation_id"] < 8
    ), "SPEAD substation must be less than 8"
    vc_lookup = int(header["csp_substation_id"])
    assert (
        header["csp_station_id"] < 1024
    ), "SPEAD station id must be less than 1024"
    vc_lookup += int(header["csp_station_id"]) * (2**3)
    assert (
        header["csp_channel_beam_id"] < 16
    ), "SPEAD beam id must be less than 16"
    vc_lookup += int(header["csp_channel_beam_id"]) * (2**13)
    assert (
        header["csp_channel_frequency_id"] < 512
    ), "SPEAD frequency id must be less than 512"
    vc_lookup += int(header["csp_channel_frequency_id"]) * (2**17)
    assert (
        header["csp_subarray_id"] < 32
    ), "SPEAD subarray id must be less than 32"
    vc_lookup += int(header["csp_subarray_id"]) * (2**26)
    # set valid
    vc_lookup += 2**31
    return vc_lookup


def decode_ethernet(packets, vc_table) -> np.ndarray:
    """
    Decodes ethernet packets for signal processing.

    :param packets: an iterable, each element representing one packet
    :param vc_table: virtual channel table.
    :return: array[virtual_channel][polarisation][time], each element an LFAA
    sample.
    """
    print(f"📨 Decoding {len(packets)} ethernet packets")
    # pre-allocate array to hold ALL sample values
    # Extract the total number of virtual channels from vc_table.
    # Every second element of vc_table is a virtual channel.
    # Virtual channels are only valid if the associated channel info in the
    # previous word has the top bit set.
    max_vc = 0
    vc_used = np.zeros(2048)
    for vc_entry in range(0, vc_table.size, 2):
        # top bit in the vc_table indicates that the entry is used.
        if vc_table[vc_entry] & 0x80000000:
            this_vc = vc_table[vc_entry + 1]
            vc_used[this_vc] = 1
            if this_vc > max_vc:
                max_vc = this_vc

    total_vc = np.sum(vc_used)
    # Every second entry in the VC table,
    # i.e. the entries with info about the packets
    vc_table_lookup = vc_table[0::2]
    # odd indexed entries in the VC table, i.e. the virtual channel numbers.
    vc_table_vc = vc_table[1::2]
    # Assume all the virtual channels have the same amount of time in the input
    # data. Use the total number of packets in the input to determine how many
    # packets there are per virtual channel.
    # 2048 time samples per packet.
    total_time_samples = int(2048 * np.ceil(len(packets) / total_vc))
    # virtual channel runs from 0 to max_vc inclusive,
    # so the array size needs to be max_vc+1
    samples = np.zeros(
        (max_vc + 1, 2, total_time_samples), dtype=lfaa_spead.complex_int8
    )

    # Array to keep track of the number of packets for each virtual channel.
    packet_count = np.zeros((1024), dtype=int)
    # Each packet is 2048 samples x 2 polarisations
    packet_samples_shape = (lfaa_spead.SAMPLES_PER_PACKET, 2)

    skipped_count = 0
    total_packets_processed = 0
    for packet in packets:
        # raw returns a tuple for each packet,
        # with the list of bytes and the time it is meant to be sent.
        eth = dpkt.ethernet.Ethernet(bytes(packet[0]))
        if (
            eth.type == dpkt.ethernet.ETH_TYPE_IP
            and eth.data.p == dpkt.ip.IP_PROTO_UDP
        ):
            # TODO: should we check that the packet destination address matches
            #  our address?
            # ethernet -> IP -> UDP -> UDP payload
            udp_payload = eth.data.data.data
            header = np.frombuffer(
                udp_payload[: lfaa_spead_header.itemsize],
                dtype=lfaa_spead_header,
            )
            # get the rest of the packet from the end of the spead header on.
            lfaa_payload = udp_payload[lfaa_spead_header.itemsize :]
            vc_lookup = check_lfaa_header(header, lfaa_payload)
            vc_index = np.argwhere(vc_table_lookup == vc_lookup)
            this_packet_count = int(header["packet_counter"])
            if len(vc_index) == 1:
                # Found the matching virtual channel
                this_vc = int(vc_table_vc[vc_index])
                # Decode into data
                packet_samples = (
                    np.frombuffer(lfaa_payload, dtype=complex_int8)
                    .reshape(packet_samples_shape)
                    .T
                )
                # Put into the full data array, assuming that the packet count
                # starts from 0 in the incoming data stream.
                samples[
                    this_vc,
                    :,
                    this_packet_count
                    * 2048 : (this_packet_count * 2048 + 2048),
                ] = packet_samples

                packet_count[this_vc] += 1
                total_packets_processed += 1
            else:
                print("No matching virtual channel!")
        else:
            skipped_count += 1
    print(f"\tDecoded {np.sum(packet_count)} packets, skipped {skipped_count}")
    print(
        f"\tTotal time samples = {total_time_samples}, corresponding to"
        f" {total_time_samples * 1080e-9} seconds of data"
    )
    return samples


def write_tb_axi(
    packets,
    test_vector_file: typing.TextIO,
    fast_packets,
    max_packets,
    eth_slow_rate,
) -> int:
    """
    Takes a list of ethernet packets and writes them out as a text file suitable for reading by the testbench
    Each line of the text file has hex strings for fields
     repeat axi_tvalid axi_tlast axi_tkeep axi_tdata axi_tuser
    where
     repeat = Number of times to send the same line (useful for inserting gaps in the axi data stream)
     axi_tvalid is 0 or 1, the tvalid axi port
     axi_tlast is 0 or 1, the tlast axi port
     axi_tkeep is 16 hex characters (i.e. 64 bits) to indicate whether to keep each byte in axi_tdata
     axi_tdata is 128 hex characters (i.e. 64 bytes) with the axi_tdata field.
      The first byte in the packet is left-aligned.
     axi_tuser is 20 hex characters with the timestamp for the packet.

    :param packets: an iterable, each element representing one packet
    :param test_vector_file: Text file to write the output to.
    :param fast_packets: Number of packets to send at the full 100GE rate.
      remaining packets are sent at a reduced rate to avoid overflowing the filterbank capacity.
    :param max_packets: maximum number of packets to write to the text file.
    :param eth_slow_rate : Data rate to use to send packets after all the fast_packets have been sent.
      note fast_packets are sent at 100GE line rate.
    :return: 1 to indicate success
    """
    total_packets = len(packets)
    if max_packets < total_packets:
        packets_to_write = max_packets
        print(
            f"📨 Writing out all the ethernet packets to the axi testbench text file (total number of packets = {packets_to_write})"
        )
    else:
        packets_to_write = total_packets
        print(
            f"📨 Writing out {packets_to_write} ethernet packets to axi testbench text file"
        )

    # Array to keep track of the number of packets for each virtual channel.
    # packet_count = np.zeros((1024), dtype=int)
    # Each packet is 2048 samples x 2 polarisations
    # packet_samples_shape = (lfaa_spead.SAMPLES_PER_PACKET, 2)
    total_packets_processed = 0
    for packet in packets:
        if total_packets_processed >= packets_to_write:
            break
        # raw returns a tuple for each packet,
        # with the list of bytes and the time it is meant to be sent.
        eth_bytes = bytes(packet[0])
        eth_length = len(eth_bytes)
        idles_sent = 0
        if total_packets_processed < fast_packets:
            # Add lines of zeros to emulate 100GE data rate
            # 64 bytes per line at 100Gbit/sec = 5.12 ns per line.
            # 100GE core uses a 322 MHz clock, so 3.105 ns per clock.
            # Average extra lines per used line is (5.12 - 3.105) / 3.105
            idles_per_512 = (512.0 / ETH_RATE) / (1000.0 / ETH_CLK) - 1
        else:
            # reduce to a bit under 1/4 line rate for remaining packets.
            # Actual rate required to avoid overflowing the filterbank processing rate
            # can be a bit less than this for small numbers of stations due to
            # the processing of groups of 4 channels in parallel - so, e.g. processing
            # 1 station takes the same time as processing 4 stations in the most extreme case.
            idles_per_512 = (512.0 / (eth_slow_rate)) / (1000.0 / ETH_CLK) - 1

        data_lines = math.ceil(eth_length / 64.0)
        last_line_bytes = eth_length - (data_lines - 1) * 64
        for packet_line in range(data_lines):

            while idles_sent < (packet_line * idles_per_512):
                idles_sent += 1
                test_vector_file.write(
                    "0000 0 0 0000000000000000 FEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5EFEEDBA5E 01230123456789ABCDEF\n"
                )

            # repeats
            test_vector_file.write("0000 ")
            # tvalid
            test_vector_file.write("1 ")
            if packet_line == (data_lines - 1):
                # Last line of data
                # tlast
                test_vector_file.write("1 ")
                # tkeep - keep last_line_bytes
                binary_string = "1" * last_line_bytes + "0" * (
                    64 - last_line_bytes
                )
                hex_string = f"{int(binary_string,2):X} "
                test_vector_file.write(hex_string)
                # tdata
                test_vector_file.write(
                    eth_bytes[
                        (packet_line * 64) : (
                            packet_line * 64 + last_line_bytes
                        )
                    ].hex()
                )
                test_vector_file.write("00" * (64 - last_line_bytes))
            else:
                # tlast
                test_vector_file.write("0 ")
                # tkeep
                test_vector_file.write("FFFFFFFFFFFFFFFF ")
                # tdata
                test_vector_file.write(
                    eth_bytes[
                        (packet_line * 64) : (packet_line * 64 + 64)
                    ].hex()
                )

            # tuser (packet timestamp)
            test_vector_file.write(" 12341234432112344321")
            test_vector_file.write("\n")
        total_packets_processed += 1

    return 1
