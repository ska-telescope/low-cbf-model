# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# All rights reserved

from scapy.all import IntField, Packet, ShortField, XByteField


class SpeadInitialisation(Packet):
    """ " This class represents a typical SPEAD initilisation packet for LOW CBF to SDP"""

    name = "SPEAD Initialisation Packet LOW CBF to SDP"
    # generate field addresses in steps of 136 (0x88) - TODO: why 136?
    field_pointer = iter(range(8, 9000, 136))
    fields_desc = [
        XByteField("magic", 0x53),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),  # 0x00
        ShortField(
            "NumberOfItems", 35
        ),  # 5 mandatory + number of Low CBF descriptor (31 now)
        ShortField("HeapCounteHdr", 0x8001),  # 0x8001
        IntField("SubArray_Beam_Frequency", 1),
        ShortField("integrationID", 0),
        ShortField("HeapSizeHdr", 0x8002),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 864),
        ShortField("HeapOffsetHdr", 0x8003),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField(
            "HeapPayloadHdr", 0x8004
        ),  # 0x8004, first bit = 1, then 0x0004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 872),
        # Now we add all the Item descriptor Items in group of 3 Fields
        # fields 1 and 2 are standard
        # field 3 should contain the first byte of the payload (0) and then
        # each subsequent field 3 increments by the size of each spead packet
        ShortField("ItemDescriptorEpoch", 5),  # 0x0005
        ShortField("reserved_descriptor_Epoch", 0),
        # TODO - there was a "To be computed" comment on the first
        #  "Address_of_pointer_..." when all the values were hard-coded, so
        #  maybe 0 is not the right starting point of field_pointer ?
        IntField("Address_of_pointer_Epoch", next(field_pointer)),
        # 0x0005, first bit = 0 then 0x0005
        ShortField("ItemDescriptortOffs", 0x0005),
        ShortField("reserved_descriptor_tOffs", 0),
        IntField("Address_of_pointer_tOffs", next(field_pointer)),
        ShortField("ItemDescriptorChann", 0x0005),
        ShortField("reserved_descriptor_Chann", 0),
        IntField("Address_of_pointer_Chann", next(field_pointer)),
        ShortField("ItemDescriptorBasel", 0x0005),
        ShortField("reserved_descriptor_Basel", 0),
        IntField("Address_of_pointer_Basel", next(field_pointer)),
        ShortField("ItemDescriptorScan", 0x0005),
        ShortField("reserved_descriptor_Scan", 0),
        IntField("Address_of_pointer_Scan", next(field_pointer)),
        ShortField("ItemDescriptorSrcID", 0x0005),
        ShortField("reserved_descriptor_SrcID", 0),
        IntField("Address_of_pointer_SrcID", next(field_pointer)),
        ShortField("ItemDescriptorHardw", 0x0005),
        ShortField("reserved_descriptor_Hardw", 0),
        IntField("Address_of_pointer_Hardw", next(field_pointer)),
        ShortField("ItemDescriptorBeaID", 0x0005),
        ShortField("reserved_descriptor_BeaID", 0),
        IntField("Address_of_pointer_BeaID", next(field_pointer)),
        ShortField("ItemDescriptorSubar", 0x0005),
        ShortField("reserved_descriptor_Subar", 0),
        IntField("Address_of_pointer_Subar", next(field_pointer)),
        ShortField("ItemDescriptorIntegration", 0x0005),
        ShortField("reserved_descriptor_Integration", 0),
        IntField("Address_of_pointer_Integration", next(field_pointer)),
        ShortField("ItemDescriptorResolution", 0x0005),
        ShortField("reserved_descriptor_Resolution", 0),
        IntField("Address_of_pointer_Resolution", next(field_pointer)),
        ShortField("ItemDescriptorResol", 0x0005),
        ShortField("reserved_descriptor_Resol", 0),
        IntField("Address_of_pointer_Resol", next(field_pointer)),
        ShortField("ItemDescriptorFrequency_Hz", 0x0005),
        ShortField("reserved_descriptor_Frequency_Hz", 0),
        IntField("Address_of_pointer_Frequency_Hz", next(field_pointer)),
        ShortField("ItemDescriptorZoomID", 0x0005),
        ShortField("reserved_descriptor_ZoomID", 0),
        IntField("Address_of_pointer_ZoomID", next(field_pointer)),
        ShortField("ItemDescriptorFirmware", 0x0005),
        ShortField("reserved_descriptor_Firmware", 0),
        IntField("Address_of_pointer_Firmware", next(field_pointer)),
        ShortField("ItemDescriptorCorrelation", 0x0005),
        ShortField("reserved_descriptor_Correlation", 0),
        IntField("Address_of_pointer_Correlation", next(field_pointer)),
        ShortField("TimeSPSHdr", 0xE013),  # 0xE000
        ShortField("reserved_TimeSPS", 0),
        IntField("TimeSPS", 1),
        ShortField("ChannelHdr", 0xE002),  # 0xE002
        ShortField("reserved_Channel", 0),
        IntField("Channel", 1),
        ShortField("BaselineHdr", 0xE005),  # 0xE005
        ShortField("reserved_Baseline", 0),
        IntField("Baseline", 1),
        ShortField("ScanHdr", 0x6008),  # 0x6008
        ShortField("reserved_Scan", 0),
        IntField("ScanIDPointer", 0),
        ShortField("HardwareHdr", 0xE009),  # 0xE009
        ShortField("reserved_Hardware", 0),
        IntField("Hardware", 1),
        ShortField("BeamHdr", 0xE00B),  # 0xE00B
        ShortField("reserved_Beam", 0),
        IntField("Beam", 1),
        ShortField("SubarrayHdr", 0xE00C),  # 0xE00C
        ShortField("reserved_Subarray", 0),
        IntField("Subarray", 1),
        ShortField("IntegrationHdr", 0xE00D),  # 0xE00D
        ShortField("reserved_Integration", 0),
        IntField("Integration", 1),
        ShortField("FrequencyHdr", 0xE00E),  # 0xE00E
        ShortField("reserved_Frequency", 0),
        IntField("Frequency", 1),
        ShortField("ResolutionHdr", 0xE00F),  # 0xE00F
        ShortField("reserved_Resolution", 0),
        IntField("Resolution", 1),
        ShortField("FrequencyHzHdr", 0xE010),  # 0xE010
        ShortField("reserved_FrequencyHz", 0),
        IntField("FrequencyHz", 1),
        ShortField("ZoomHdr", 0xE011),  # 0xE011
        ShortField("reserved_zoom", 0),
        IntField("ZoomID", 1),
        ShortField("FirmwareHdr", 0xE012),  # 0xE011
        ShortField("reserved_Firmware", 0),
        IntField("Firmware", 0x01020304),
        ShortField("SourceIDHdr", 0xE015),
        ShortField("reserved_SourceID", 0),
        IntField("SourceID", ord("L")),
        # ShortField("CorrelationHdr", 0x600A),  # 0x600A
        # ShortField("reserved_Correlation", 0),
        # IntField("CorrelationPointer", 16),
        ShortField("StreamControlHdr", 0x8006),
        ShortField("reserved_control", 0),
        IntField("StreamControl", 0),
    ]


class SpeadDescriptor(Packet):
    """ " This class represents a typical SPEAD descriptor packet"""

    name = "SPEAD descriptor packet "
    fields_desc = [
        XByteField("magic", 0x53),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),  # 0x00
        ShortField("NumberOfItems", 9),  # 0x0F
        ShortField("HeapCounteHdr", 0x8001),  # 0x8001
        IntField("SubArray_Beam_Frequency", 0),
        ShortField("integrationID", 1),
        ShortField("HeapSizeHdr", 0x8002),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 8),
        ShortField("HeapOffsetHdr", 0x8003),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField("HeapPayloadHdr", 0x8004),  # 0x8004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 8),
        ShortField("IdentifierHdr", 0x8014),  # 0x8014
        ShortField("reserved_identifier", 0),
        IntField("Identifier", 1),  # New identifier header
        ShortField("NameHdr", 0x0010),  # 0x0010
        ShortField("reserved_name", 0),
        IntField("PointerToName", 0),
        ShortField("ShapeHdr", 0x0013),  # 0x0012
        ShortField("reserved_Shape", 0),
        IntField("PointerToShape", 5),
        ShortField("TypeHdr", 0x0013),  # 0x0013
        ShortField("reserved_type", 0),
        IntField("PointerToType", 5),
        ShortField("NumpyTypeHdr", 0x0015),  # 0x0015
        ShortField("reserved_NumpyType", 0),
        IntField("PointerToNumpyType", 5),
    ]


class SpeadSDP(Packet):
    """ " This class represents a typical SPEAD data packet for LOW CBF to SDP"""

    name = "SPEAD LOW CBF to SDP packet"
    fields_desc = [
        XByteField("magic", 0x53),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),
        ShortField(
            "NumberOfItems", 6
        ),  # 4 mandatory + number of Low CBF descriptor (2 now)
        ShortField("HeapCounteHdr", 0x8001),  # 0x8001
        IntField("SubArray_Beam_Frequency", 0),
        ShortField("integrationID", 1),
        ShortField("HeapSizeHdr", 0x8002),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 8),
        ShortField("HeapOffsetHdr", 0x8003),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField("HeapPayloadHdr", 0x8004),  # 0x8004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 8),
        # Now it is about SDP only
        # ShortField("TimeSPSHdr", 0xE013),  # 0xE000
        # ShortField("reserved_TimeSPS", 0),
        # IntField("TimeSPS", 1),
        ShortField("TimeSPSOffsetHdr", 0x6014),  # 0xE001
        ShortField("reserved_TimeSPSOffset", 0),
        IntField("TimeSPSOffset", 0),
        # ShortField("ChannelHdr", 0xE002),  # 0xE002
        # ShortField("reserved_Channel", 0),
        # IntField("Channel", 1),
        # ShortField("BaselineHdr", 0xE005),  # 0xE005
        # ShortField("reserved_Baseline", 0),
        # IntField("Baseline", 1),
        # ShortField("ScanHdr", 0x6008),  # 0x6008
        # ShortField("reserved_Scan", 0),
        # IntField("ScanIDPointer", 0),
        # ShortField("HardwareHdr", 0xE009),  # 0xE009
        # ShortField("reserved_Hardware", 0),
        # ntField("Hardware", 1),
        # ShortField("BeamHdr", 0xE00B),  # 0xE00B
        # ShortField("reserved_Beam", 0),
        # IntField("Beam", 1),
        # ShortField("SubarrayHdr", 0xE00C),  # 0xE00C
        # ShortField("reserved_Subarray", 0),
        # IntField("Subarray", 1),
        # ShortField("IntegrationHdr", 0xE00D),  # 0xE00D
        # ShortField("reserved_Integration", 0),
        # IntField("Integration", 1),
        # ShortField("FrequencyHdr", 0xE00E),  # 0xE00E
        # ShortField("reserved_Frequency", 0),
        # IntField("Frequency", 1),
        # ShortField("ResolutionHdr", 0xE00F),  # 0xE00F
        # ShortField("reserved_Resolution", 0),
        # IntField("Resolution", 1),
        # ShortField("FrequencyHzHdr", 0xE010),  # 0xE010
        # ShortField("reserved_FrequencyHz", 0),
        # IntField("FrequencyHz", 1),
        # ShortField("ZoomHdr", 0xE011),  # 0xE011
        # ShortField("reserved_zoom", 0),
        # IntField("ZoomID", 1),
        # ShortField("FirmwareHdr", 0xE012),  # 0xE011
        # ShortField("reserved_Firmware", 0),
        # IntField("Firmware", 0x01020304),
        # ShortField("SourceIDHdr", 0xE015),  # 0xE011
        # ShortField("reserved_SourceID", 0),
        # IntField("SourceID", ord("L")),
        ShortField("CorrelationHdr", 0x600A),  # 0x600A
        ShortField("reserved_Correlation", 0),
        IntField("CorrelationPointer", 8),
    ]


class SpeadEnd(Packet):
    """ " This class represents a typical SPEAD termination packet for LOW CBF to SDP"""

    name = "SPEAD Termination Packet for LOW CBF to SDP"
    fields_desc = [
        XByteField("magic", 83),  # 0x53
        XByteField("version", 4),  # 0x04
        XByteField("ItemPointerWidth", 2),  # 0x02
        XByteField("HeapAddWidth", 6),  # 0x06
        ShortField("Reserved", 0),  # 0x00
        ShortField("NumberOfItems", 5),  # 0x0F
        ShortField("HeapCounteHdr", 0x8001),  # 0x8001 10000001
        IntField("SubArray_Beam_Frequency", 0),
        ShortField("integrationID", 1),
        ShortField("HeapSizeHdr", 0x8002),  # 0x8002
        ShortField("reserved_size", 0),
        IntField("HeapSize", 0),
        ShortField("HeapOffsetHdr", 0x8003),  # 0x8003
        ShortField("reserved_offset", 0),
        IntField("OffsetSize", 0),
        ShortField("HeapPayloadHdr", 0x8004),  # 0x8004
        ShortField("reserved_payload", 0),
        IntField("PayloadSize", 0),
        ShortField("StreamControlHdr", 0x8006),  # 0x8006
        ShortField("reserved_control", 0),
        IntField("StreamControl", 2),
    ]
