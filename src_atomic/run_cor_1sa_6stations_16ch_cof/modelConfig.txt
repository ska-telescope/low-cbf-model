{
   "SNR" : [101],
   "time" : [0],
   "runtime" : [10],
   "dataTime" : [10],
   "configuration" : [400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415],
   "runCorrelator" : [1],
   "runPSS" : [0],
   "runPST" : [0],
   "delayUpdatePeriod" : [1],
   "useLocalDoppler" : [0],
   "forceDelayZero" : [0]
}
