function result = sinc(x)
  if (nargin ~= 1)
    print_usage ();
  end
  result = ones (size (x));
  i = (x ~= 0);
  if (any (i(:)))
    t = pi * x(i);
    result(i) = sin (t) ./ t;
  end
end