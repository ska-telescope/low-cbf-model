% Compare model and wireshark data.
% Wireshark data is saved in four csv files
% apol_re.txt, apol_im.txt, bpol_re.txt, bpol_im.txt
clear all;

a_re = table2array(readtable('apol_re.txt'));
a_im = table2array(readtable('apol_im.txt'));
b_re = table2array(readtable('bpol_re.txt'));
b_im = table2array(readtable('bpol_im.txt'));

fw_samples = size(a_re,1);
fw_freqs = size(a_re,2);
freqTimeWS = zeros(fw_freqs,fw_samples,2,1);  % 4-D so it is the same as the model output.
freqTimeWS(:,:,1,1) = a_re.' + 1i * a_im.';
freqTimeWS(:,:,2,1) = b_re.' + 1i * b_im.';

% get freqTimeML, the output of the matlab model.
% Has dimensions (frequency channels, time samples, pol, beam)
load('../PST_results.mat') 

figure(1);
clf;
plot(abs(shiftdim(freqTimeWS(100,1:200,2))),'b.-');
hold on;
grid on;
plot(abs(shiftdim(freqTimeML(100,1:96,2,1))),'go');
