function write_tb_data(rundir,LRUSelect, maxPackets, maxGap)
    % write testbench data for simulation testing of a module
    % Inputs
    %  rundir : the directory containing the configuration files for this run
    %  lruSelect : Which LRU to generate the register setting files for.
    %  maxPackets : Maximum number of data packets to put in the testbench files (otherwise files can be very large and take a very long time to simulate)
    %  maxGap : maximum gap between packets in clock cycles, to avoid very 
    %           long simulation times with no activity.
    %           the minimum value of maxGap for 1 pipeline should be around
    %           768 to avoid the overflow of coarse corner turn, this
    %           number can be scaled by the number of pipelines, so if 3
    %           pipelines, the minimum value of maxGap is 768/3 = 256
    % Outputs:
    %  (1) Text files with lines of the form 
    %       >[args_register_name][offset] 
    %       >value0
    %       >value1 etc.
    %      Notes:
    %       * "offset" is a word offset from the address referenced by "args_register_name",
    %         and value0, value1 etc are 32 bit data words in hexadecimal.
    %       * The data for this file is compiled into the matlab structure "regwrites" and written to text files at the end of this function.
    %       * A separate file is produced for each update of the registers 
    %        (e.g. for local Doppler, there are multiple different sets of values as the doppler correction is updated).
    %  (2) VHDL testbench package with a procedure that generates AXI4-lite transactions to write register values.
    %      Uses two functions from the axi4_lite_pkg (defined in libraries/base/axi4/src/vhdl/axi4_lite_pkg.vhd)
    %       - axi_lite_transaction()  - for single register writes
    %       - axi_lite_blockwrite()   - for writing to memories.
    %  (3) Text files with values used by the axi_lite_blockwrite function.
    %

%     if exist("OCTAVE_VERSION", "builtin") > 0
%         octave_reg = loadjson([rundir '/registerSettings.txt']);
%         for ii = 1:length(octave_reg.LRU)
%             registers.LRU(ii) = octave_reg.LRU{ii};
%         end
%         registers.global = octave_reg.global;
%     else
%         fid = fopen([rundir '/registerSettings.txt']);
%         regjson = fread(fid,inf);
%         fclose(fid);
%         regjson = char(regjson');
%         registers = jsondecode(regjson);
%     end

    modelConfig = parseModelConfig(rundir);
%     
%     doDoppler = false;
%     
%     %keyboard
%     
%     %% Setup for the file to write data to for upload to the real hardware.
%     numberOfUpdates = size(registers.LRU(LRUSelect).LDcountOffset,2);
%     rCount = zeros(1,numberOfUpdates);  % Number of register writes for each file.
%     
%     %% Configuration of the LFAADecode module
%     
%     % Virtual channel table
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'lfaadecode100g.statctrl.vctable';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = registers.LRU(LRUSelect).virtualChannel;
%     
%     % Total number of stations
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'lfaadecode100g.statctrl.total_stations';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = registers.LRU(LRUSelect).total_stations;
% 
%     % Total number of different coarse channels
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'lfaadecode100g.statctrl.total_coarse';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = registers.LRU(LRUSelect).total_coarse;
%     
%     % Total number of virtual channels
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'lfaadecode100g.statctrl.total_channels';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = registers.LRU(LRUSelect).total_channels;
% 
%     %% Jones matrices 
%     % For the test bench, write Jones matrices for two beamformers.
%     % For the real world, generate Jones matrix data for 64 beamformers.
%     %
%     % Each beamformer has 64K of address space.
%     %  0-32K = Jones Matrices
%     %          - Two buffers of 16K each.       
%     %  32-64K = Phase tracking
%     %          - Two buffers of 16K each.
%     % Addressing is bytes, so each beamformer has 64k/4 = 16384 x 32bit words of configuration data.
%     NBEAMS = 2;
%     for beam = 1:NBEAMS
%         rCount(1) = rCount(1) + 1;
%         regWrites{1}(rCount(1)).name = 'pstbeamformer.pstbeamformer.data';
%         regWrites{1}(rCount(1)).addr = 65536 * (beam-1);
%         regWrites{1}(rCount(1)).data = round(rand(16384*NBEAMS,1) * (2^32 - 1));
%     end
%     
%     %% Configure the first stage corner turn
%     % untimed frame count start register - set to zero to start as soon as we receive something for the next buffer.
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'ct_atomic_pst_in.config.untimed_framecount_start';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = 0;
%     % Set corner turn reset
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'ct_atomic_pst_in.config.full_reset';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = 1;
%     % Clear corner turn reset
%     rCount(1) = rCount(1) + 1;
%     regWrites{1}(rCount(1)).name = 'ct_atomic_pst_in.config.full_reset';
%     regWrites{1}(rCount(1)).addr = 0;
%     regWrites{1}(rCount(1)).data = 0;
%     
%     
%     %% Configure the local Doppler module
%     % Loop through the updates
%     if (doDoppler)
%         numberOfUpdates = size(registers.LRU(LRUSelect).LDcountOffset,2);
%         for update = 1:numberOfUpdates
%             if (modelConfig.useLocalDoppler)
%                 rCount(update) = rCount(update) + 1;
%                 regWrites{update}(rCount(update)).name = 'localDoppler_countOffset';
%                 if (mod(update,2) == 1)
%                     regWrites{update}(rCount(update)).addr = 0;
%                 else
%                     regWrites{update}(rCount(update)).addr = 768;
%                 end
%                 regWrites{update}(rCount(update)).data = registers.LRU(LRUSelect).LDcountOffset(:,update);
%             end
% 
%             if (modelConfig.useLocalDoppler)
%                 rCount(update) = rCount(update) + 1;
%                 regWrites{update}(rCount(update)).name = 'localDoppler_startPhase';
%                 if (mod(update,2) == 1)
%                     regWrites{update}(rCount(update)).addr = 0;
%                 else
%                     regWrites{update}(rCount(update)).addr = 1536;
%                 end
%                 regWrites{update}(rCount(update)).data = registers.LRU(LRUSelect).LDstartPhase(:,update);
%                 
%                 rCount(update) = rCount(update) + 1;
%                 regWrites{update}(rCount(update)).name = 'localDoppler_phaseStep';
%                 if (mod(update,2) == 1)
%                     regWrites{update}(rCount(update)).addr = 0;
%                 else
%                     regWrites{update}(rCount(update)).addr = 1536;
%                 end
%                 regWrites{update}(rCount(update)).data = registers.LRU(LRUSelect).LDphaseStep(:,update);
%             end
% 
%         end
%     end
%     
%     %% Coarse and Fine Delay
%     if (1==0)
%         for update = 1:numberOfUpdates
%             rCount(update) = rCount(update) + 1;
%             if (mod(update,2) == 1)
%                 regWrites{update}(rCount(update)).name = 'ctcconfig.coarse_delay.table_0';
%             else
%                 regWrites{update}(rCount(update)).name = 'ctcconfig.coarse_delay.table_1';
%             end
%             regWrites{update}(rCount(update)).addr = 0;
%             for n1 = 1:1536
%                 r1 = registers.LRU(LRUSelect).CCTdelayTable(n1,update);
%                 if (r1 < 0)
%                     r1 = r1 + 2^32; % Convert to a negative 32 bit 2s complement value.
%                 end
%                 regWrites{update}(rCount(update)).data(n1) = r1;
%             end
%         end
%     end
    
    %% Write out the files for uploading to the real hardware.
    % This is now done in "create_config.m"
%     for update = 1:numberOfUpdates  % step through, one file for each register update.
%          fid = fopen([rundir '/tb/HWData' num2str(update) '.txt'],'wt');
%          for writeCount = 1:rCount(update)  % step through each register that we are writing to in this update.
%              fprintf(fid,['[' regWrites{update}(writeCount).name '][' num2str(regWrites{update}(writeCount).addr) ']\n']);
%              for n1 = 1:length(regWrites{update}(writeCount).data)
%                  fprintf(fid,['0x' dec2hex(regWrites{update}(writeCount).data(n1),8) '\n']);
%              end
%          end
%          fclose(fid);
%     end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Data buses as text files for supplying and checking data in the testbench
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% - 100GE LFAA data 
    %   * Includes MAC addresses
    %   * Does not include the FCS
    %
    % The 100GE interface testbench uses a text file where each line has the following fields, 
    % each represented with the given number of hex digits :
    %  
    % data[128] valid[1] eop[1] error[1] empty0[1] empty1[1] empty2[1] empty3[1] sop[1]
    % 
    if exist("OCTAVE_VERSION", "builtin") == 0
        rng(1); % Ensures a repeatable set of random numbers
    end
    fname = [rundir '/LFAA.mat'];
    if exist(fullfile(pwd,rundir,'LFAA.mat'),'file')
        % fpga is an array of structures, with each structure having fields
        % .headers, .data, .dataPointers, .txTimes
        load(fname,'fpga')
    else
        error(['Cannot find ' fname '. Run create_config.m to generate it']);
    end
    totalPackets = min([maxPackets,size(fpga(LRUSelect).headers,2)]);
    pkt = zeros([8320,1],'uint8');  % 8306 bytes in the packet, plus an extra 14 to get to a 16 byte boundary.
    % tuser will be 
    % first word = 0b0000011 = ena, sop
    % last word = 0b0110101 = ena, eop, mty = 6
    % remaining words = 0b0000001 = ena only.
    tuser = zeros([1039,1]);
    tuser(1) = 3;
    tuser(2:1038) = 1;
    tuser(1039) = 53;
    
    fid = fopen([rundir '/tb/LFAA100GE_tb_data.txt'],'wt');
    f_saxi = fopen([rundir '/tb/LFAA100GE_s_axi_tb_data.txt'],'wt');
    previousTime = fpga(1).txTimes(1);
    packetClocks = 0;
    for p1 = 1:totalPackets
        
        % Compile a set of 16 byte words for this packet
        
        %  - Header is 114 bytes; first 112 bytes in 7 x (16 byte words)
        %  - Last 2 bytes of the header, then 14 bytes of data
        %  - 511 x (16 byte words) of data; 
        %  - Final 2 bytes of data, + 14 bytes of padding.
        %   = total of 8320 bytes (including the 14 bytes of padding to get to a 16 byte alignment)
        %   = 520 x (16 byte words)
        
        pkt(1:114) = fpga(LRUSelect).headers(:,p1);
        ptr = fpga(LRUSelect).dataPointers(p1,:);  % ptr is 2 element vector, 1st element is offset within the channel (with 1 based indexing), second is the channel, with channel 0 meaning data is all zeros.
        if (ptr(2) == 0)
            pkt(115:8306) = 0;
        else
            r1 = int32(real(fpga(LRUSelect).data(ptr(1):(ptr(1)+4095),ptr(2))));
            i1 = int32(imag(fpga(LRUSelect).data(ptr(1):(ptr(1)+4095),ptr(2))));
            for c1 = 1:4096
                if (r1(c1) < 0)
                    pkt(115 + 2*(c1-1)) = uint8(r1(c1) + 256);
                else
                    pkt(115 + 2*(c1-1)) = uint8(r1(c1));
                end
                if (i1(c1) < 0)
                    pkt(116 + 2*(c1-1)) = uint8(i1(c1) + 256);
                else
                    pkt(116 + 2*(c1-1)) = uint8(i1(c1));
                end
            end
        end
        pkt(8307:8320) = 0;
        
        % Build pkt into blocks of 16 byte words
        % wfull layout is the LBUS approach
        % wfull is for streaming AXI, the layout is byte reversed to the
        % RTL s_axi implementation, it will present like
        % reading a PCAP in wireshark. TB will byte swap the S-AXI.

        for w = 1:520
            wfull(w,1:16)  = [dec2hex(pkt((w-1)*16+1),2) dec2hex(pkt((w-1)*16+2),2) dec2hex(pkt((w-1)*16+3),2) dec2hex(pkt((w-1)*16+4),2) dec2hex(pkt((w-1)*16+5),2) dec2hex(pkt((w-1)*16+6),2) dec2hex(pkt((w-1)*16+7),2) dec2hex(pkt((w-1)*16+8),2)];
            wfull(w,17:32) = [dec2hex(pkt((w-1)*16+9),2) dec2hex(pkt((w-1)*16+10),2) dec2hex(pkt((w-1)*16+11),2) dec2hex(pkt((w-1)*16+12),2) dec2hex(pkt((w-1)*16+13),2) dec2hex(pkt((w-1)*16+14),2) dec2hex(pkt((w-1)*16+15),2) dec2hex(pkt((w-1)*16+16),2)];
        end
        
        % Insert the gap between packets
        currentTime = fpga(1).txTimes(p1);
        % The LFAA 100GE interface operates at 322 MHz internally, with 64 bytes per clock.
        % currentTime and previousTime are in ns.
        % At 322 MHz, clock period is 3.1 ns.
        % In the simulation, we have 2 beamformers, so we need enough time per frame for e.g.
        %   4 stations, 2 channels, 780 clocks per packet, 400MHz clocks (2.5 ns/clock), 9 packets/channel ( = 216 fine channels/24 fine channels per packet)
        %  = (2 channels) * (9 packets/channel) * (2 beams) * (780 clocks/packet) * (2.5 ns/clock)
        %  = 70 us
        % squash the first 32 packets to reduce the simulation time
        if (p1 < 32)
            maxGapFinal = 100;
        else
            maxGapFinal = maxGap;
        end
        gapTime = (currentTime - previousTime) - packetClocks * 3.1;
        if (gapTime < 12)
            gapClocks = 4;
        elseif ((gapTime/3.2) > maxGapFinal)
            gapClocks = maxGapFinal;
        else
            gapClocks = floor(gapTime/3.2);
        end

        fprintf(fid, [dec2hex(gapClocks,4) ' 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 0 0 0 0 0 0 0 0\n']);
        fprintf(f_saxi, [dec2hex(gapClocks,4) ' 0 0 0000000000000000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\n']); % data_valid, last, valid_bytes, data
        packetClocks = 0;
        if (1 == 0) 
            % Start the packet in the second, third or fourth segment
            
        else
            % Start the packet in the first segment, last line will have all segments used, but only 2 bytes used in the final segment
            dline = 1;
            fprintf(fid,['0000 ' wfull((dline-1)*4+4,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+1,:) ' F 0 0 0 0 0 0 1\n']);  % Valid, all emptys = 0 (i.e. all bytes used), start of packet set for segment 0
            fprintf(f_saxi,['0000 1 0 FFFFFFFFFFFFFFFF ' wfull((dline-1)*4+1,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+4,:) '\n']);  % data_valid, last, valid_bytes, data
            packetClocks = packetClocks + 1;
            if (rand(1) > 0.5)  % Insert an idle cycle
                fprintf(fid,['0000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 0 0 0 0 0 0 0 0\n']);
                fprintf(f_saxi,['0000 0 0 0000000000000000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\n']); % data_valid, last, valid_bytes, data
                packetClocks = packetClocks + 1;
            end
            dline = 2;
            fprintf(fid,['0000 ' wfull((dline-1)*4+4,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+1,:) ' F 0 0 0 0 0 0 0\n']);  % Valid, all bytes used, not sop or eop
            fprintf(f_saxi,['0000 1 0 FFFFFFFFFFFFFFFF ' wfull((dline-1)*4+1,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+4,:) '\n']);
            packetClocks = packetClocks + 1;
            for dline = 3:129
                fprintf(fid,['0000 ' wfull((dline-1)*4+4,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+1,:) ' F 0 0 0 0 0 0 0\n']);  % Valid, all bytes used, not sop or eop
                fprintf(f_saxi,['0000 1 0 FFFFFFFFFFFFFFFF ' wfull((dline-1)*4+1,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+4,:) '\n']);
                packetClocks = packetClocks + 1;
                if (rand(1) > 0.5) % Insert an idle cycle
                    fprintf(fid,['0000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 0 0 0 0 0 0 0 0\n']);
                    fprintf(f_saxi,['0000 0 0 0000000000000000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\n']); % data_valid, last, valid_bytes, data
                    packetClocks = packetClocks + 1;
                end
            end
            dline = 130;
            fprintf(fid,['0000 ' wfull((dline-1)*4+4,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+1,:) ' F 8 0 0 0 0 E 0\n']);  % Valid, all bytes used except in last segment where 14 bytes are unused, 2 used. eop in final segment
            fprintf(f_saxi,['0000 1 1 FFFFFFFFFFFFC000 ' wfull((dline-1)*4+1,:) wfull((dline-1)*4+2,:) wfull((dline-1)*4+3,:) wfull((dline-1)*4+4,:) '\n']);
            packetClocks = packetClocks + 1;
            fprintf(fid,['0000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 0 0 0 0 0 0 0 0\n']);
            fprintf(f_saxi,['0000 0 0 0000000000000000 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\n']); % data_valid, last, valid_bytes, data
            packetClocks = packetClocks + 1;
        end
        
        % update curTime with the size of the gap between packets,
        previousTime = currentTime;
    end
    fclose(fid);
    fclose(f_saxi);
    
