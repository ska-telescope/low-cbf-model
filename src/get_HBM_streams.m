function get_HBM_streams(rundir,fpga,integration)
% inputs
%  rundir - model run
%  fpga - which LRU to generate corner turner data for (e.g. for PISA, 1 to 3)
%  integration - which integration period to generate data for (each period is 0.9 seconds) (1 to however many frames have been produced)
%
% Loads stage1 model output, and generates a set of data streams for each station + virtual channel combination used in the corner turn
% Assumes PISA configuration (3 FPGAs, every 3rd virtual channel goes to a particular FPGA)
% So there are 128 virtual channels in the corner turn, and 6 stations.
%
fname = [rundir '/stage1.mat'];

USE_LFAA_DATA_FROM = 1;  % Only use LFAA data sourced from this FPGA - assumes only one FPGA has valid input.
INCLUDE_DOPPLER = 0;     % include doppler processing; if not, then use the LFAA ingest data instead of the stage1 output data.

if exist(fullfile(pwd,rundir,'stage1.mat'),'file')
    load(fname);
else
    error(['Cannot find ' fname '. Run run_model.m to generate it']);
end

% Generate a text file with an array of pointers to the location in the data file where each virtual channel + station starts.
%
% Text file will have :
%
% 128 rows - one for each virtual channel that goes to this coarse corner turn.
% 6 columns - one for each station that goes to this coarse corner turn.
% 
% Each entry in the array is the starting point in the data file to get the data that is needed, or 0 if the channel is all zeros.
% Note the data file is just an array of bytes, with all channels concatenated together.

ptrs = zeros(128,6);
for vc = 0:127   % 128 virtual channels in each corner turn
    for station = 1:6   % 6 stations in each corner turn
        % Find the first occurence of this virtual channel and station in the stage1 data
        i1 = find((stage1(USE_LFAA_DATA_FROM).headers(1,:) == (3*vc + fpga - 1)) & (stage1(USE_LFAA_DATA_FROM).headers(3,:) == station),1);
        dp = stage1(USE_LFAA_DATA_FROM).dataPointers(i1,:);
        if isempty(dp)
            % No data for this stream
            ptrs(vc+1,station) = -1;
        else
            % Found data, check if the data is zeros
            if (dp(2) == 0)  % data is zero
                ptrs(vc+1,station) = 0;
            else
                % 408 packets * 2048 time samples per packet * 4 (2 bytes per complex sample, 2 pol), 
                % Note polarisations are interleaved in the data.
                % dp(1) is the offset in the data stream; this should be 1 since we selected the first packet for this virtual channel and station
                % dp(2) is the stream.
                ptrs(vc+1,station) = (dp(2)-1) * 408 * 2048 * 4 + 1;
            end
        end
    end
end

fnamewrcsv = [rundir '/HBMStream_fpga' num2str(fpga) '.txt'];
dlmwrite(fnamewrcsv,ptrs,'precision','%10.0f');


% Extract just this integration period, and write to a file.
fnamewr = [rundir '/HBMStream_fpga' num2str(fpga) '.raw'];

if (INCLUDE_DOPPLER == 1)
    data = stage1(USE_LFAA_DATA_FROM).data;
else
    fname = [rundir '/LFAA.mat'];
    load(fname);  % WARNING - overwrites "fpga"
    data = fpga(USE_LFAA_DATA_FROM).data;
end


fid = fopen(fnamewr,'w');
s1 = size(data);

samplesPerChannel = 2048*408*2;
bytesPerChannel = 2*2048*408*2; % 2 bytes(complex), 2048 per packet, 408 packets per integration period, 2 polarisations.

thisChannel = zeros(bytesPerChannel,1);
for b1 = 1:s1(2)
    thisChannel(1:2:end) = real(data((samplesPerChannel*(integration-1)+1):(samplesPerChannel*(integration-1)+samplesPerChannel),b1));  % Note fpga(XX).data is complex int8 data.
    thisChannel(2:2:end) = imag(data((samplesPerChannel*(integration-1)+1):(samplesPerChannel*(integration-1)+samplesPerChannel),b1));
    fwrite(fid,thisChannel,'int8');
end
fclose(fid);

